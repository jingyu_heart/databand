package org.databandtech.flinkstreaming;

import org.apache.flink.api.common.functions.MapFunction;
import org.databandtech.flinkstreaming.Entity.EpgVod;

import com.alibaba.fastjson.JSONObject;


public class KafkaConsumerMapper implements MapFunction<String,EpgVod> {
	
	private static final long serialVersionUID = -4315037274258625042L;

	@Override
	public EpgVod map(String value) throws Exception {
		EpgVod model = JSONObject.parseObject(value,EpgVod.class);
		return model;
	}



}
