import { getDicts,getDictsSync} from "@/api/system/dict/data";

export default {
   options(h, conf, key) {

    if (conf.__config__.ryType === 'dict') //若依字典列表
    {
      const list = []
      let data = JSON.parse(getDictsSync(conf.__slot__[key]))
      let opts = data.data
        opts.forEach(item => {
          console.log(item.dictLabel +' - '+ item.dictValue +' - '+ item.status)
          list.push(<el-option label={item.dictLabel} value={item.dictValue}></el-option>)
        })
        return list
      }  
    else {//普通列表
      const list = []
      conf.__slot__.options.forEach(item => {
        list.push(<el-option label={item.label} value={item.value} disabled={item.disabled}></el-option>)
      })

      list.push(<el-option label='bb' value='b'></el-option>)
      return list
    }

  }

}
