package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.DyformMapper;
import com.ruoyi.system.domain.Dyform;
import com.ruoyi.system.service.IDyformService;

/**
 * 动态单Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-06-11
 */
@Service
public class DyformServiceImpl implements IDyformService 
{
    @Autowired
    private DyformMapper dyformMapper;

    /**
     * 查询动态单
     * 
     * @param id 动态单ID
     * @return 动态单
     */
    @Override
    public Dyform selectDyformById(Long id)
    {
        return dyformMapper.selectDyformById(id);
    }

    /**
     * 查询动态单列表
     * 
     * @param dyform 动态单
     * @return 动态单
     */
    @Override
    public List<Dyform> selectDyformList(Dyform dyform)
    {
        return dyformMapper.selectDyformList(dyform);
    }

    /**
     * 新增动态单
     * 
     * @param dyform 动态单
     * @return 结果
     */
    @Override
    public int insertDyform(Dyform dyform)
    {
        return dyformMapper.insertDyform(dyform);
    }

    /**
     * 修改动态单
     * 
     * @param dyform 动态单
     * @return 结果
     */
    @Override
    public int updateDyform(Dyform dyform)
    {
        return dyformMapper.updateDyform(dyform);
    }

    /**
     * 批量删除动态单
     * 
     * @param ids 需要删除的动态单ID
     * @return 结果
     */
    @Override
    public int deleteDyformByIds(Long[] ids)
    {
        return dyformMapper.deleteDyformByIds(ids);
    }

    /**
     * 删除动态单信息
     * 
     * @param id 动态单ID
     * @return 结果
     */
    @Override
    public int deleteDyformById(Long id)
    {
        return dyformMapper.deleteDyformById(id);
    }
}
