package org.databandtech.flink.reducefunc;

import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.tuple.Tuple2;

public class AddReduce implements ReduceFunction<Tuple2<String, Integer>> {

	private static final long serialVersionUID = 1038798377295722017L;

	@Override
	public Tuple2<String, Integer> reduce(Tuple2<String, Integer> value1,
			Tuple2<String, Integer> value2) throws Exception {
		return Tuple2.of(value1.f0, value1.f1 + value2.f1);
	}
}
