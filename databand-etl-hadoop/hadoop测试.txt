Mapreduce 测试

1、上传测试文件到hdfs上
hadoop fs -put /home/api.txt /api.txt
2、运行MR程序
hadoop jar /usr/app/hadoop-2.7.3/share/hadoop/mapreduce/hadoop-mapreduce-examples-2.7.3.jar wordcount /api.txt /output
or
yarn jar /usr/app/hadoop-2.7.3/share/hadoop/mapreduce/hadoop-mapreduce-examples-2.7.3.jar wordcount /api1.txt /output2
3、查看结果，执行如下命令：
hadoop fs -text /output/part-r-00000
or
hadoop fs -cat /output/part-r-00000
或者下载到本地
hadoop fs -get  /output1 /home/output1

测试HDFS写性能

测试内容：向HDFS集群写10个128M的文件
hadoop jar /usr/app/hadoop-2.7.3/share/hadoop/mapreduce/hadoop-mapreduce-client-jobclient-2.7.3-tests.jar TestDFSIO -write -nrFiles 10 -fileSize 128MB
测试HDFS读性能

测试内容：读取HDFS集群10个128M的文件
hadoop jar /usr/app/hadoop-2.7.3/share/hadoop/mapreduce/hadoop-mapreduce-client-jobclient-2.7.3-tests.jar TestDFSIO -read -nrFiles 10 -fileSize 128MB

删除测试生成数据
hadoop jar /usr/app/hadoop-2.7.3/share/hadoop/mapreduce/hadoop-mapreduce-client-jobclient-2.7.3-tests.jar TestDFSIO -clean


使用Sort程序评测MapReduce

（1）使用RandomWriter来产生随机数，每个节点运行10个Map任务，每个Map产生大约1G大小的二进制随机数
hadoop jar /usr/app/hadoop-2.7.3/share/hadoop/mapreduce/hadoop-mapreduce-examples-2.7.3.jar randomwriter random-data
（2）执行Sort程序
hadoop jar /usr/app/hadoop-2.7.3/share/hadoop/mapreduce/hadoop-mapreduce-examples-2.7.3.jar sort random-data sorted-data
（3）验证数据是否真正排好序了
hadoop jar /usr/app/hadoop-2.7.3/share/hadoop/mapreduce/hadoop-mapreduce-examples-2.7.3.jar testmapredsort -sortInput random-data -sortOutput sorted-data