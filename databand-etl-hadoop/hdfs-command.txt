## HDFS 常用命令 ##

1. 显示当前目录结构

# 显示当前目录结构
hadoop fs -ls  <path>
# 递归显示当前目录结构
hadoop fs -ls  -R  <path>
# 显示根目录下内容
hadoop fs -ls  /
hadoop fs -ls /usr

2. 创建目录

# 创建目录
hadoop fs -mkdir  <path> 
# 递归创建目录
hadoop fs -mkdir -p  <path> 

Step 1
You have to create an input directory.

$ $HADOOP_HOME/bin/hadoop fs -mkdir /user/input 
$ $
hadoop fs -mkdir /usr
 
3. 删除操作

# 删除文件
hadoop fs -rm  <path>
# 递归删除目录和文件
hadoop fs -rm -R  <path> 


4. 从本地加载文件到 HDFS

# 二选一执行即可
hadoop fs -put  [localsrc] [dst] 
hadoop fs - copyFromLocal [localsrc] [dst] 

hadoop fs -put /home/file.txt /usr

5. 从 HDFS 导出文件到本地

# 二选一执行即可
hadoop fs -get  [dst] [localsrc] 
hadoop fs -copyToLocal [dst] [localsrc] 

6. 查看文件内容

# 二选一执行即可
hadoop fs -text  <path> 
hadoop fs -cat  <path>  

hadoop fs -cat /usr/core-site.xml

7. 显示文件的最后一千字节

hadoop fs -tail  <path> 
# 和Linux下一样，会持续监听文件内容变化 并显示文件的最后一千字节
hadoop fs -tail -f  <path> 

8. 拷贝文件

hadoop fs -cp [src] [dst]

9. 移动文件

hadoop fs -mv [src] [dst] 

10. 统计当前目录下各文件大小

默认单位字节
-s : 显示所有文件大小总和，
-h : 将以更友好的方式显示文件大小（例如 64.0m 而不是 67108864）
hadoop fs -du  <path>  

hadoop fs -du /usr

11. 合并下载多个文件

-nl 在每个文件的末尾添加换行符（LF）
-skip-empty-file 跳过空文件
hadoop fs -getmerge
# 示例 将HDFS上的hbase-policy.xml和hbase-site.xml文件合并后下载到本地的/usr/test.xml
hadoop fs -getmerge -nl  /test/hbase-policy.xml /test/hbase-site.xml /usr/test.xml

12. 统计文件系统的可用空间信息

hadoop fs -df -h /

13. 更改文件复制因子

hadoop fs -setrep [-R] [-w] <numReplicas> <path>
更改文件的复制因子。如果 path 是目录，则更改其下所有文件的复制因子
-w : 请求命令是否等待复制完成
# 示例
hadoop fs -setrep -w 3 /user/hadoop/dir1

14. 权限控制

# 权限控制和Linux上使用方式一致
# 变更文件或目录的所属群组。 用户必须是文件的所有者或超级用户。
hadoop fs -chgrp [-R] GROUP URI [URI ...]
# 修改文件或目录的访问权限  用户必须是文件的所有者或超级用户。
hadoop fs -chmod [-R] <MODE[,MODE]... | OCTALMODE> URI [URI ...]
# 修改文件的拥有者  用户必须是超级用户。
hadoop fs -chown [-R] [OWNER][:[GROUP]] URI [URI ]

15. 文件检测

hadoop fs -test - [defsz]  URI
可选选项：

-d：如果路径是目录，返回 0。
-e：如果路径存在，则返回 0。
-f：如果路径是文件，则返回 0。
-s：如果路径不为空，则返回 0。
-r：如果路径存在且授予读权限，则返回 0。
-w：如果路径存在且授予写入权限，则返回 0。
-z：如果文件长度为零，则返回 0。
# 示例
hadoop fs -test -e filename

16. 追加文件

appendToFile
Usage: hadoop dfs -appendToFile <localsrc> ... <dst>

追加一个或者多个文件到hdfs制定文件中.也可以从命令行读取输入.

· hdfs dfs -appendToFile localfile /user/hadoop/hadoopfile

· hdfs dfs -appendToFile localfile1 localfile2 /user/hadoop/hadoopfile

· hdfs dfs -appendToFile localfile hdfs://nn.example.com/hadoop/hadoopfile

· hdfs dfs -appendToFile - hdfs://nn.example.com/hadoop/hadoopfile Reads the input from stdin.

17. 显示剩余空间
df
Usage: hadoop fs -df [-h] URI [URI ...]
Options:

The -h option will format file sizes in a “human-readable” fashion (e.g 64.0m instead of 67108864)
Example:

hadoop dfs -df /user/hadoop/dir1


汇总: 
hadoop fs [generic options]
        [-appendToFile <localsrc> ... <dst>]
        [-cat [-ignoreCrc] <src> ...]
        [-checksum <src> ...]
        [-chgrp [-R] GROUP PATH...]
        [-chmod [-R] <MODE[,MODE]... | OCTALMODE> PATH...]
        [-chown [-R] [OWNER][:[GROUP]] PATH...]
        [-copyFromLocal [-f] [-p] [-l] [-d] <localsrc> ... <dst>]
        [-copyToLocal [-f] [-p] [-ignoreCrc] [-crc] <src> ... <localdst>]
        [-count [-q] [-h] [-v] [-t [<storage type>]] [-u] [-x] <path> ...]
        [-cp [-f] [-p | -p[topax]] [-d] <src> ... <dst>]
        [-createSnapshot <snapshotDir> [<snapshotName>]]
        [-deleteSnapshot <snapshotDir> <snapshotName>]
        [-df [-h] [<path> ...]]
        [-du [-s] [-h] [-x] <path> ...]
        [-expunge]
        [-find <path> ... <expression> ...]
        [-get [-f] [-p] [-ignoreCrc] [-crc] <src> ... <localdst>]
        [-getfacl [-R] <path>]
        [-getfattr [-R] {-n name | -d} [-e en] <path>]
        [-getmerge [-nl] [-skip-empty-file] <src> <localdst>]
        [-help [cmd ...]]
        [-ls [-C] [-d] [-h] [-q] [-R] [-t] [-S] [-r] [-u] [<path> ...]]
        [-mkdir [-p] <path> ...]
        [-moveFromLocal <localsrc> ... <dst>]
        [-moveToLocal <src> <localdst>]
        [-mv <src> ... <dst>]
        [-put [-f] [-p] [-l] [-d] <localsrc> ... <dst>]
        [-renameSnapshot <snapshotDir> <oldName> <newName>]
        [-rm [-f] [-r|-R] [-skipTrash] [-safely] <src> ...]
        [-rmdir [--ignore-fail-on-non-empty] <dir> ...]
        [-setfacl [-R] [{-b|-k} {-m|-x <acl_spec>} <path>]|[--set <acl_spec> <path>]]
        [-setfattr {-n name [-v value] | -x name} <path>]
        [-setrep [-R] [-w] <rep> <path> ...]
        [-stat [format] <path> ...]
        [-tail [-f] <file>]
        [-test -[defsz] <path>]
        [-text [-ignoreCrc] <src> ...]
        [-touchz <path> ...]
        [-truncate [-w] <length> <path> ...]
        [-usage [cmd ...]]

其他命令

18、DistCp（分布式拷贝）

DistCp（分布式拷贝）是用于大规模集群内部和集群之间拷贝的工具。 它使用Map/Reduce实现文件分发，错误处理和恢复，以及报告生成。 它把文件和目录的列表作为map任务的输入，每个任务会完成源列表中部分文件的拷贝。 由于使用了Map/Reduce方法，这个工具在语义和执行上都会有特殊的地方。 这篇文档会为常用DistCp操作提供指南并阐述它的工作模型。

使用方法
基本使用方法
DistCp最常用在集群之间的拷贝：

bash$ hadoop distcp hdfs://nn1:8020/foo/bar \
                    hdfs://nn2:8020/bar/foo

这条命令会把nn1集群的/foo/bar目录下的所有文件或目录名展开并存储到一个临时文件中，这些文件内容的拷贝工作被分配给多个map任务， 然后每个TaskTracker分别执行从nn1到nn2的拷贝操作。注意DistCp使用绝对路径进行操作。

命令行中可以指定多个源目录：

bash$ hadoop distcp hdfs://nn1:8020/foo/a \
                    hdfs://nn1:8020/foo/b \
                    hdfs://nn2:8020/bar/foo

或者使用-f选项，从文件里获得多个源：
bash$ hadoop distcp -f hdfs://nn1:8020/srclist \
                       hdfs://nn2:8020/bar/foo
其中srclist 的内容是
    hdfs://nn1:8020/foo/a
    hdfs://nn1:8020/foo/b

当从多个源拷贝时，如果两个源冲突，DistCp会停止拷贝并提示出错信息， 如果在目的位置发生冲突，会根据选项设置解决。 默认情况会跳过已经存在的目标文件（比如不用源文件做替换操作）。每次操作结束时 都会报告跳过的文件数目，但是如果某些拷贝操作失败了，但在之后的尝试成功了， 那么报告的信息可能不够精确（请参考附录）。

每个TaskTracker必须都能够与源端和目的端文件系统进行访问和交互。 对于HDFS来说，源和目的端要运行相同版本的协议或者使用向下兼容的协议。 （请参考不同版本间的拷贝 ）。

拷贝完成后，建议生成源端和目的端文件的列表，并交叉检查，来确认拷贝真正成功。 因为DistCp使用Map/Reduce和文件系统API进行操作，所以这三者或它们之间有任何问题 都会影响拷贝操作。一些Distcp命令的成功执行可以通过再次执行带-update参数的该命令来完成， 但用户在如此操作之前应该对该命令的语法很熟悉。

值得注意的是，当另一个客户端同时在向源文件写入时，拷贝很有可能会失败。 尝试覆盖HDFS上正在被写入的文件的操作也会失败。 如果一个源文件在拷贝之前被移动或删除了，拷贝失败同时输出异常 FileNotFoundException。

选项
选项索引
标识	描述	备注
-p[rbugp]	Preserve
  r: replication number
  b: block size
  u: user
  g: group
  p: permission
修改次数不会被保留。并且当指定 -update 时，更新的状态不会 被同步，除非文件大小不同（比如文件被重新创建）。
-i	忽略失败	就像在 附录中提到的，这个选项会比默认情况提供关于拷贝的更精确的统计， 同时它还将保留失败拷贝操作的日志，这些日志信息可以用于调试。最后，如果一个map失败了，但并没完成所有分块任务的尝试，这不会导致整个作业的失败。
-log <logdir>	记录日志到 <logdir>	DistCp为每个文件的每次尝试拷贝操作都记录日志，并把日志作为map的输出。 如果一个map失败了，当重新执行时这个日志不会被保留。
-m <num_maps>	同时拷贝的最大数目	指定了拷贝数据时map的数目。请注意并不是map数越多吞吐量越大。
-overwrite	覆盖目标	如果一个map失败并且没有使用-i选项，不仅仅那些拷贝失败的文件，这个分块任务中的所有文件都会被重新拷贝。 就像下面提到的，它会改变生成目标路径的语义，所以 用户要小心使用这个选项。
-update	如果源和目标的大小不一样则进行覆盖	像之前提到的，这不是"同步"操作。 执行覆盖的唯一标准是源文件和目标文件大小是否相同；如果不同，则源文件替换目标文件。 像 下面提到的，它也改变生成目标路径的语义， 用户使用要小心。
-f <urilist_uri>	使用<urilist_uri> 作为源文件列表	这等价于把所有文件名列在命令行中。 urilist_uri 列表应该是完整合法的URI。
更新和覆盖
这里给出一些 -update和 -overwrite的例子。 考虑一个从/foo/a 和 /foo/b 到 /bar/foo的拷贝，源路径包括：

    hdfs://nn1:8020/foo/a
    hdfs://nn1:8020/foo/a/aa
    hdfs://nn1:8020/foo/a/ab
    hdfs://nn1:8020/foo/b
    hdfs://nn1:8020/foo/b/ba
    hdfs://nn1:8020/foo/b/ab

如果没设置-update或 -overwrite选项， 那么两个源都会映射到目标端的 /bar/foo/ab。 如果设置了这两个选项，每个源目录的内容都会和目标目录的 内容 做比较。DistCp碰到这类冲突的情况会终止操作并退出。

默认情况下，/bar/foo/a 和 /bar/foo/b 目录都会被创建，所以并不会有冲突。

现在考虑一个使用-update合法的操作:
distcp -update hdfs://nn1:8020/foo/a \
               hdfs://nn1:8020/foo/b \
               hdfs://nn2:8020/bar

其中源路径/大小:

    hdfs://nn1:8020/foo/a
    hdfs://nn1:8020/foo/a/aa 32
    hdfs://nn1:8020/foo/a/ab 32
    hdfs://nn1:8020/foo/b
    hdfs://nn1:8020/foo/b/ba 64
    hdfs://nn1:8020/foo/b/bb 32

和目的路径/大小:

    hdfs://nn2:8020/bar
    hdfs://nn2:8020/bar/aa 32
    hdfs://nn2:8020/bar/ba 32
    hdfs://nn2:8020/bar/bb 64

会产生:

    hdfs://nn2:8020/bar
    hdfs://nn2:8020/bar/aa 32
    hdfs://nn2:8020/bar/ab 32
    hdfs://nn2:8020/bar/ba 64
    hdfs://nn2:8020/bar/bb 32

只有nn2的aa文件没有被覆盖。如果指定了 -overwrite选项，所有文件都会被覆盖。

19、hadoop调用任何类

hadoop脚本可用于调调用任何类。

用法：hadoop CLASSNAME

运行名字为CLASSNAME的类。

hadoop CLASSNAME(如：hadoop org.apache.hadoop.test.HelloWorld)
实际上真正运行，需要配置$HADOOP_CLASSPATH变量。举例说明：

export HADOOP_CLASSPATH=放置class文件的目录
如：export HADOOP_CLASSPATH=/mnt/clazz
再执行类即可。

hadoop org.apache.hadoop.test.HelloWorld

20、运行jar
Usage: hadoop jar <jar> [mainClass] args...

Runs a jar file.

或者YARN替代
http://hadoop.apache.org/docs/r2.9.2/hadoop-yarn/hadoop-yarn-site/YarnCommands.html#jar

Usage: yarn jar <jar> [mainClass] args...
Runs a jar file. Users can bundle their YARN code in a jar file and execute it using this command.

更多
http://hadoop.apache.org/docs/r2.9.2/hadoop-project-dist/hadoop-common/CommandsManual.html